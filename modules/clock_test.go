package modules

import (
	"testing"
	"os"
	"image/png"
	"time"
	"github.com/skip2/go-qrcode"
	"encoding/json"
	"github.com/gorilla/websocket"
	"encoding/base64"
	"errors"
)

type Message struct {
	Path   string          `json:"path"`
	Method string          `json:"method"`
	State  string          `json:"state,omitempty"`
	Data   json.RawMessage `json:"data,omitempty"`
}

func sendMessage(msg *Message) ([]byte, error) {
	c, _, err := websocket.DefaultDialer.Dial("ws://widora.local/homemaster", nil)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	data, _ := json.Marshal(msg)
	err = c.WriteMessage(websocket.TextMessage, data)
	if err != nil {
		return nil, err
	}

	c.SetReadDeadline(time.Now().Add(time.Second * 3))
	_, recv, err := c.ReadMessage()
	if err != nil {
		return nil, err
	}

	c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	return recv, nil
}

func TestClockDisplayIcon(t *testing.T) {
	t.Log("let's go!")

	f, err := os.Open("../build/files/etc/homemaster/src/daodao.png")
	if err != nil {
		t.Fatal(err)
	}
	defer f.Close()

	img, err := png.Decode(f)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("image width(%d) height(%d)\n", img.Bounds().Max.X, img.Bounds().Max.Y)

	// encode
	data := make([]byte, 128*128/2)
	for i := 0; i < img.Bounds().Max.X; i++ {
		for j := 0; j < img.Bounds().Max.Y; j += 2 {
			r1, _, _, _ := img.At(j, i).RGBA()
			r2, _, _, _ := img.At(j+1, i).RGBA()
			r1 = r1 & 0xFF * 7 / 255
			r2 = r2 & 0xFF * 7 / 255
			data[i*64+j/2] = byte(r1 + (r2&0x0F)<<4)
		}
	}

	base64Data := base64.StdEncoding.EncodeToString(data)
	oled := map[string]interface{}{
		"topic": "oled",
		"data":  base64Data,
	}

	msg := &Message{
		Path:   "test",
		Method: "post",
	}
	msg.Data, _ = json.Marshal(oled)
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestClockScreenTime(t *testing.T) {
	clock := clockScreen{
		time:        time.Now(),
		temperature: 23,
		humidity:    83,
		brightness:  6,
	}

	data, err := clock.encode()
	if err != nil {
		t.Fatal(err)
	}

	base64Data := base64.StdEncoding.EncodeToString(data)
	oled := map[string]interface{}{
		"topic": "oled",
		"data":  base64Data,
	}

	msg := &Message{
		Path:   "test",
		Method: "post",
	}
	msg.Data, _ = json.Marshal(oled)
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestQRCode(t *testing.T) {
	err := qrcode.WriteFile(`{"ap":"homemaster","psd":"howoldareyou"}`, qrcode.Medium, 182, "/Users/yun/Desktop/homemaster.png")
	if err != nil {
		t.Fatal(err)
	}
}

type oledWifiCharInfo struct {
	x      int
	y      int
	width  int
	height int
}

func (t *oledWifiCharInfo) getCode() ([]byte, error) {
	f, err := os.Open("../build/files/etc/homemaster/src/wifi.png")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	img, err := png.Decode(f)
	if err != nil {
		return nil, err
	}

	if t.x < 0 || t.y < 0 ||
		t.x > img.Bounds().Max.X || t.y > img.Bounds().Max.Y ||
		t.x+t.width > img.Bounds().Max.X || t.y+t.height > img.Bounds().Max.Y {
		return nil, errors.New("out of range")
	}

	buffer := make([]byte, t.width*t.height)
	for i := 0; i < t.height; i++ {
		for j := 0; j < t.width; j ++ {
			bit, _, _, _ := img.At(j+t.x, i+t.y).RGBA()
			buffer[i*t.width+j] = byte(bit & 0xff * oledLightnessMax / 255)
		}
	}

	code := make([]byte, oledWidth*oledHeight/2)
	for i := 0; i < oledHeight; i++ {
		for j := 0; j < oledWidth; j += 2 {
			code[(i*oledWidth+j)/2] = buffer[i*128+j]*4/oledLightnessMax +
				((buffer[i*128+j+1]*4/oledLightnessMax)&0x0F)<<4
		}
	}
	return code, nil
}

func TestQRCodeDisplay(t *testing.T) {
	for i := 0; i < 90; i++ {
		info := oledWifiCharInfo{
			x:      4 * 128,
			y:      0,
			width:  128,
			height: 128,
		}
		data, err := info.getCode()
		if err != nil {
			t.Fatal(err)
		}
		base64Data := base64.StdEncoding.EncodeToString(data)
		oled := map[string]interface{}{
			"topic": "oled",
			"data":  base64Data,
		}

		msg := &Message{
			Path:   "test",
			Method: "post",
		}
		msg.Data, _ = json.Marshal(oled)
		recv, err := sendMessage(msg)
		if err != nil {
			t.Fatal(err)
		}
		t.Log("recv: ", string(recv))

		time.Sleep(time.Millisecond * 600)
	}
}


func TestClockScreenWifiConnecting(t *testing.T) {
	for i:=0; i<60; i++ {
		clock := clockScreen{
			brightness: 6,
			wifi: "danke",
			progress: i % 4,
		}

		data, err := clock.encodeWifi()
		if err != nil {
			t.Fatal(err)
		}

		base64Data := base64.StdEncoding.EncodeToString(data)
		oled := map[string]interface{}{
			"topic": "oled",
			"data":  base64Data,
		}

		msg := &Message{
			Path:   "test",
			Method: "post",
		}
		msg.Data, _ = json.Marshal(oled)
		recv, err := sendMessage(msg)
		if err != nil {
			t.Fatal(err)
		}
		t.Log("recv: ", string(recv))

		time.Sleep(time.Millisecond * 500)
	}

}


