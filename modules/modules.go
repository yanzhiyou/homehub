package modules

import (
	"time"
)

func Start() error {
	err := MasterOpen()
	if err != nil {
		return err
	}

	StartClock()
	StartHTMeasure()
	StartLightMeasure()
	time.Sleep(time.Microsecond * 10)

	StartWifiManager()
	return nil
}

func Stop() {
	StopClock()
	StopHTMeasure()
	StopLightMeasure()
	//MasterClose()
}