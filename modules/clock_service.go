package modules

import (
	"adai.design/homemaster/container/service"
	"adai.design/homemaster/container"
	"adai.design/homemaster/container/characteristic"
	"github.com/gorilla/websocket"
)

const ClockSevId = 3

var clockService = service.NewClock()

func init() {
	container.AddService(clockService.Service, ClockSevId)
	clockService.SetName("Clock")

	clockService.Volume.OnValueUpdateFromConn(func(conn *websocket.Conn, c *characteristic.Characteristic, newValue, oldValue interface{}) {
		clockService.Volume.UpdateValue(newValue)
	})

	clockService.Brightness.OnValueUpdateFromConn(func(conn *websocket.Conn, c *characteristic.Characteristic, newValue, oldValue interface{}) {
		clockService.Volume.UpdateValue(newValue)
	})
}

