package modules

import (
	"adai.design/homemaster/container/service"
	"adai.design/homemaster/container"
)

const LightSensorSevId = 2

var lightService = service.NewLightSensor()

func init() {
	container.AddService(lightService.Service, LightSensorSevId)
	lightService.SetName("LightSensor")
}