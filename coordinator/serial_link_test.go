package coordinator

import (
	"testing"
	"adai.design/homemaster/log"
	"encoding/binary"
	"time"
	"encoding/hex"
	"bytes"
	"strings"
	"fmt"
)




func TestGetVersion(t *testing.T) {
	msg := &Message{MsgT: SerialLinkMsgTypeGetVersion, Data: nil}
	sdata, err := msg.Encode()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("send: %x\n", sdata)

	result, err := sendData(sdata)
	if err != nil {
		t.Fatal(err)
	}

	packets := bytes.Split(result, []byte{0x03})

	for i, v := range packets {
		if len(v) > 0 {
			t.Logf("packet[%d]: %x", i, append(v, 0x03))
			msg := newMessage(append(v, 0x03))
			if msg != nil {
				t.Logf("msg(%04x) data(%x)", msg.MsgT, msg.Data)
			}
		}
	}
}

func TestReadAttributeRequest(t *testing.T) {
	str := "02 930A 01 01 0000 00 00 0000 01 0004"
	str = strings.Replace(str, " ", "", -1)
	data, _ := hex.DecodeString(str)
	msg := &Message{MsgT: SerialLinkMsgTypeReadAttributeRequest, Data: data}
	sdata, err := msg.Encode()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("send: %x\n", sdata)

	result, err := sendData(sdata)
	if err != nil {
		t.Fatal(err)
	}

	packets := bytes.Split(result, []byte{0x03})

	for i, v := range packets {
		if len(v) > 0 {
			t.Logf("packet[%d]: %x", i, append(v, 0x03))
			msg := newMessage(append(v, 0x03))
			if msg != nil {
				t.Logf("msg(%04x) data(%x)", msg.MsgT, msg.Data)
			}
		}
	}
}

func TestGoMoveToLevel(t *testing.T) {
	level := 60
	str := "02 E9D9 01 0B 00 " + fmt.Sprintf("%02x", level * 255 / 100) + " 0000"
	str = strings.Replace(str, " ", "", -1)
	data, _ := hex.DecodeString(str)
	msg := &Message{MsgT: SerialLinkMsgTypeMoveToLevel, Data: data}
	sdata, err := msg.Encode()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("send: %x\n", sdata)

	result, err := sendData(sdata)
	if err != nil {
		t.Fatal(err)
	}
	packets := bytes.Split(result, []byte{0x03})
	for i, v := range packets {
		if len(v) > 0 {
			t.Logf("packet[%d]: %x", i, append(v, 0x03))
			msg := newMessage(append(v, 0x03))
			if msg != nil {
				t.Logf("msg(%04x) data(%x)", msg.MsgT, msg.Data)
			}
		}
	}
}

func TestMoveToLevel(t *testing.T) {
	level := 0
	str := "02 07F6 01 0B 00 " + fmt.Sprintf("%02x", level * 255 / 100) + " 0001"
	str = strings.Replace(str, " ", "", -1)
	data, _ := hex.DecodeString(str)
	msg := &Message{MsgT: SerialLinkMsgTypeMoveToLevel, Data: data}
	sdata, err := msg.Encode()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("send: %x\n", sdata)

	result, err := sendData(sdata)
	if err != nil {
		t.Fatal(err)
	}
	packets := bytes.Split(result, []byte{0x03})
	for i, v := range packets {
		if len(v) > 0 {
			t.Logf("packet[%d]: %x", i, append(v, 0x03))
			msg := newMessage(append(v, 0x03))
			if msg != nil {
				t.Logf("msg(%04x) data(%x)", msg.MsgT, msg.Data)
			}
		}
	}
}

func TestGoMoveToColor(t *testing.T) {
	x, y := rgbToCIEXY(255, 255, 255)
	str := "02 E9D9 01 0B " + fmt.Sprintf("%04x", x) + fmt.Sprintf("%04x", y) + "0000"
	str = strings.Replace(str, " ", "", -1)
	data, _ := hex.DecodeString(str)
	msg := &Message{MsgT: SerialLinkMsgTypeMoveToColour, Data: data}
	sdata, err := msg.Encode()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("send: %x\n", sdata)

	result, err := sendData(sdata)
	if err != nil {
		t.Fatal(err)
	}
	packets := bytes.Split(result, []byte{0x03})
	for i, v := range packets {
		if len(v) > 0 {
			t.Logf("packet[%d]: %x", i, append(v, 0x03))
			msg := newMessage(append(v, 0x03))
			if msg != nil {
				t.Logf("msg(%04x) data(%x)", msg.MsgT, msg.Data)
			}
		}
	}
}

func TestGoTurnOff(t *testing.T) {
	str := "02 E9D9 01 0B 01"
	str = strings.Replace(str, " ", "", -1)
	data, _ := hex.DecodeString(str)
	msg := &Message{MsgT: SerialLinkMsgTypeOnOffNoEffects, Data: data}
	sdata, err := msg.Encode()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("send: %x\n", sdata)

	result, err := sendData(sdata)
	if err != nil {
		t.Fatal(err)
	}
	packets := bytes.Split(result, []byte{0x03})
	for i, v := range packets {
		if len(v) > 0 {
			t.Logf("packet[%d]: %x", i, append(v, 0x03))
			msg := newMessage(append(v, 0x03))
			if msg != nil {
				t.Logf("msg(%04x) data(%x)", msg.MsgT, msg.Data)
			}
		}
	}
}

func TestMoveToColor(t *testing.T) {
	x, y := rgbToCIEXY(0, 20, 255)
	str := "02 07F6 01 0B " + fmt.Sprintf("%04x", x) + fmt.Sprintf("%04x", y) + "0001"
	str = strings.Replace(str, " ", "", -1)
	data, _ := hex.DecodeString(str)
	msg := &Message{MsgT: SerialLinkMsgTypeMoveToColour, Data: data}
	sdata, err := msg.Encode()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("send: %x\n", sdata)

	result, err := sendData(sdata)
	if err != nil {
		t.Fatal(err)
	}
	packets := bytes.Split(result, []byte{0x03})
	for i, v := range packets {
		if len(v) > 0 {
			t.Logf("packet[%d]: %x", i, append(v, 0x03))
			msg := newMessage(append(v, 0x03))
			if msg != nil {
				t.Logf("msg(%04x) data(%x)", msg.MsgT, msg.Data)
			}
		}
	}
}

func TestNetWorkState(t *testing.T) {
	msg := &Message{MsgT: SerialLinkMsgTypeNetworkStateReq, Data: nil}
	sdata, err := msg.Encode()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("send: %x\n", sdata)

	result, err := sendData(sdata)
	if err != nil {
		t.Fatal(err)
	}

	packets := bytes.Split(result, []byte{0x03})
	for i, v := range packets {
		if len(v) > 0 {
			t.Logf("packet[%d]: %x", i, append(v, 0x03))
			msg := newMessage(append(v, 0x03))
			if msg != nil {
				t.Logf("msg(%04x) data(%x)", msg.MsgT, msg.Data)
			}
		}
	}
	t.Logf("result: %x", result)
}

func lightOn(netAddr uint16, endpoint uint8) error {
	data := make([]byte, 6)
	data[0] = 0x02
	binary.BigEndian.PutUint16(data[1:], netAddr)
	data[3] = 0x01
	data[4] = endpoint
	data[5] = 0x01
	msg := &Message{MsgT: SerialLinkMsgTypeOnOffNoEffects, Data: data}
	sdata, err := msg.Encode()
	if err != nil {
		return err
	}
	log.Printf("send: %x\n", sdata)

	result, err := sendData(sdata)
	if err != nil {
		return err
	}
	log.Printf("result: %x\n", result)
	return nil
}

func lightOff(netAddr uint16, endpoint uint8) error {
	//data := make([]byte, 6)
	//data[0] = 0x02
	//binary.BigEndian.PutUint16(data[1:], netAddr)
	//data[3] = 0x01
	//data[4] = endpoint
	//data[5] = 0x00
	//msg := &Message{MsgT: SerialLinkMsgTypeOnOffNoEffects, Data: data}
	//sdata, err := msg.Encode()
	//if err != nil {
	//	return err
	//}
	//log.Printf("send: %x\n", sdata)
	//result, err := sendData(sdata)
	//if err != nil {
	//	return err
	//}
	//log.Printf("result: %x\n", result)
	return nil
}

func TestTestLightOn(t *testing.T) {
	lightOn(0x37F7, 1)
	time.Sleep(500 * time.Millisecond)
	lightOn(0x37F7, 2)
	time.Sleep(500 * time.Millisecond)
	lightOn(0x5373, 1)
	time.Sleep(500 * time.Millisecond)
	lightOn(0x5373, 2)
}

func TestTestLightOff(t *testing.T) {
	lightOff(0x37F7, 1)
	time.Sleep(500 * time.Millisecond)
	lightOff(0x37F7, 2)
	time.Sleep(500 * time.Millisecond)
	lightOff(0x5373, 1)
	time.Sleep(500 * time.Millisecond)
	lightOff(0x5373, 2)
}

func TestTestSW3Off(t *testing.T) {
	lightOff(0x7c3b, 1)
	time.Sleep(500 * time.Millisecond)
	lightOff(0x7c3b, 2)
	time.Sleep(500 * time.Millisecond)
	lightOff(0x7c3b, 3)
	time.Sleep(500 * time.Millisecond)
}

func TestTestSW3On(t *testing.T) {
	lightOn(0x7c3b, 1)
	time.Sleep(500 * time.Millisecond)
	lightOn(0x7c3b, 2)
	time.Sleep(500 * time.Millisecond)
	lightOn(0x7c3b, 3)
	time.Sleep(500 * time.Millisecond)
}


func TestMessageDecode(t *testing.T) {
	buf, err := hex.DecodeString("018102120210021cf9247c3b021302100216021002101002100211021103")
	if err != nil {
		t.Fatal(err)
	}
	data := make([]byte, len(buf))
	j := 0
	for i:=0; i<len(buf); i++ {
		if buf[i] == 0x02 {
			i ++
			data[j] = buf[i] & 0x0F
			j ++
		} else {
			data[j] = buf[i]
			j ++
		}
	}
	t.Logf("%x", data[:j])
}

func TestFormatter(t *testing.T) {
	buf, err := hex.DecodeString("00158D0001D254C9")
	if err == nil {
		t.Logf("%x", buf)
	}
}

