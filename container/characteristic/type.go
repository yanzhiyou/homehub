package characteristic

import "encoding/base64"

type String struct {
	*Characteristic
}

func NewString(typ string) *String {
	char := NewCharacteristic(typ)
	char.Format = FormatString
	return &String{char}
}

func (s *String) SetValue(str string) {
	s.UpdateValue(str)
}

func (s *String) GetValue() string {
	return s.Value.(string)
}


type Int struct {
	*Characteristic
}

func NewInt(typ string) *Int {
	number := NewCharacteristic(typ)
	return &Int{number}
}

func (i *Int) SetValue(value int) {
	i.UpdateValue(value)
}

func (i *Int) GetValue() int {
	return i.Value.(int)
}


type Float struct {
	*Characteristic
}

func NewFloat(typ string) *Float {
	number := NewCharacteristic(typ)
	number.Format = FormatFloat
	return &Float{number}
}

func (c *Float) SetValue(value float32) {
	c.UpdateValue(value)
}

func (c *Float) GetValue() float32 {
	return c.Value.(float32)
}


type Bool struct {
	*Characteristic
}

func NewBool(typ string) *Bool {
	number := NewCharacteristic(typ)
	number.Format = FormatBool
	return &Bool{number}
}

func (c *Bool) SetValue(value bool) {
	c.UpdateValue(value)
}

func (c *Bool) GetValue() bool {
	return c.Value.(bool)
}


type Bytes struct {
	*String
}

func NewBytes(typ string) *Bytes {
	s := NewString(typ)
	s.Format = FormatTLV8
	return &Bytes{s}
}

func (bs *Bytes) SetValue(b []byte) {
	bs.String.SetValue(base64.StdEncoding.EncodeToString(b))
}

func (bs *Bytes) GetValue() []byte {
	str := bs.String.GetValue()
	if b, err := base64.StdEncoding.DecodeString(str); err != nil {
		return []byte{}
	} else {
		return b
	}
}












